#!/usr/bin/python2

import os
import glob

watchDir = '<path to working directory>'                        # Path to working directory
watchFiles = '<working directory>*.txt'                         # Files to look for
fileList = glob.glob(watchFiles)                                # Initial file list creation

os.chdir(watchDir)
while (len(fileList) > 10):                                     # Change the number to however many files you want to keep.
    print('Removing excess Files')
    os.remove(min(fileList, key=os.path.getctime))
    fileList = glob.glob(watchFiles)                            # Generates an updated list
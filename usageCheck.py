#!/bin/python2

import os
import os.path
import subprocess

for root, dirs, files in os.walk('<path to working directory>'):                    # Path to working directory
    checkSize = sum(os.path.getsize(os.path.join(root, name)) for name in files)

if checkSize > 2 * (1024 ** 3):
    subprocess.check_call(["service", "network", "restart"])                        # Service to restart
else:
    print('Current usage is acceptable level')

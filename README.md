# pythonExercise
This is just a repo to hold scripts I write as I learn and practice Python.

## cleanUp.py
This script will scan a specific directory and delete all but the 10 newest files. The number of files saved can be easily modified. I wrote it to be run by a cron job 

## Fibonacci.py
I saw the original code on [python.org](https://www.python.org/) for the fibonacci sequence. I decided to take it and try myself. The original has the number hard coded into the script but I wanted to be able to pass any number to it as an argument. It took me a little while to figure out the argument was being passed as a string and not an interger and the script would loop forever.

## fortuneCookie.py
Everyone tells me there's already a fortune program in Linux, but I wanted to use the collection of fortunes piling up on my desk and write my own as practice. I ended up only spending about 5 minutes on the actual code, and a few hours typing in all the fortunes.

## usageCheck.py
A team at my work runs an app that sends files to a home server and the service regularly crashes and the files build up and fill the file system. They told me to stop deleting the files. I told them give me instructions what they want me to do. This script checks if the folder is filling up and then restarts the service. The network service is in the script as an example.